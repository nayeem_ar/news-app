import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  constructor(private http: HttpClient) { }

  getData(pageNumber?: number): Observable<any>{
    return this.http.get(`${environment.baseUrl}authors?limit=10&skip=${pageNumber ? pageNumber*10 : 0}`)
  }

  // getPreviousData(): Observable<any>{
  //   return this.http.get(`${environment.baseUrl}authors?limit=10&skip=${pageNumber ? pageNumber*10 : 0}`)
  // }
}
