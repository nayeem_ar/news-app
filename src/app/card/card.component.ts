import { Component, Input, OnInit } from '@angular/core';
import { Author } from '../author.model';
import { BlogPostCard } from '../blog-post-card.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() data!: Author;
  favoriteAuthors: any[] = [];
  constructor() {}

  ngOnInit(): void {}

  addToFavorite(author: Author){
    console.log('author', author);
    this.favoriteAuthors.unshift(author);
    console.log('favoriteAuthors', this.favoriteAuthors);
    // localStorage.setItem("names", JSON.stringify(this.favoriteAuthors));
    // localStorage.setItem('favoriteAuthors', this.favoriteAuthors);

  }

  removeFromFavorite(author: Author){
    console.log('added', author);

  }
}
