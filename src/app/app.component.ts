import { BreakpointObserver } from '@angular/cdk/layout';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Author } from './author.model';
import { AuthorService } from './author/author.service';
import { BlogPostCard } from './blog-post-card.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'NewsApp';
  authors!: Author[];
  pageNumber = 0;
  posts: BlogPostCard[] = [
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life. Freelencing can be a great way to experienc true freedom in your life. Freelencing can be a great way to experienc true freedom in your life. Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
    {
      mainImageUrl: 'https://source.unsplash.com/cGwfkwHmt98/400x250',
      category: 'freelencing',
      title: 'The Road to Freedom',
      description:
        'Freelencing can be a great way to experienc true freedom in your life',
      authorImageUrl: 'https://source.unsplash.com/7YVZYZeITc8/30x30',
      authorName: 'John Smith',
      postDate: '10th August 2022',
    },
  ];

  @ViewChild(MatSidenav) sideNav!: MatSidenav;

  constructor(private service: AuthorService) {}

  ngOnInit(): void {
    this.service.getData().subscribe((res) => {
      console.log('res', res);
      this.authors = res.results;
    });
  }

  onNext(): void {
    this.pageNumber += 1;
    console.log('next', this.pageNumber);
    this.service.getData(this.pageNumber).subscribe(res=> {
      console.log(res);
      this.authors = res.results;
    })
  }

  onPrev(): void {
    this.pageNumber === 0 ? this.pageNumber = 0 : this.pageNumber -= 1;
    console.log('previous', this.pageNumber);
    this.service.getData(this.pageNumber).subscribe(res=> {
      console.log(res);
      this.authors = res.results;
    })

  }

  // ngAfterViewInit(): void {
  //   this.sideNav.opened = true;
  //   this.observer.observe(['(max-width:800px)'])
  //   .subscribe((res)=>{
  //     if(res?.matches){
  //       this.sideNav.mode="over";
  //       this.sideNav.close();
  //     }else{
  //       this.sideNav.mode = 'side';
  //       this.sideNav.open();
  //     }
  //   })
  //   this.cdr.detectChanges();
  // }
}
