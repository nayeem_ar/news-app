export interface Author {
  name: string;
  bio: string;
  dateAdded: string;
  description: string;
  link: string;
}
